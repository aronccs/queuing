<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Manage Counters</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
  <link rel="icon" href="<?php echo base_url().'images/favicon.ico';?>">
  <script src="<?= base_url().'scripts/jquery-2.2.0.js'?>" type="text/javascript"></script>
    
  <?php if(validation_errors() && $this->session->flashdata('isAdd')) { ?>
      <script type="text/javascript">
          $(window).load(function(){
              $('#addModal').modal('show');
          }); 
      </script>
  <?php } ?>
  <?php if(validation_errors() && $this->session->flashdata('isEdit')) { ?>
      <script type="text/javascript">
          $(window).load(function(){
              $('#editModal').modal('show');
          }); 
      </script>
  <?php } ?>

  <script>
    $(document).on("click", ".open-editModal", function () {
       var counter_id = $(this).data('id');
       var counter_name = $(this).data('name');
       $(".modal-body #counter_id").val( counter_id );
       $(".modal-body #counter_name").val( counter_name );
  });

    $(document).on("click", ".open-resetPassModal", function () {
       var manager = $(this).data('id');
       $(".modal-body #counter_manager").val( manager );
  });

    $(document).on("click", ".open-deleteModal", function () {
       var counter_id = $(this).data('id');
       $(".modal-body #del_counter_id").val( counter_id );
  });
  </script>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Queuing</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url().'queuing/admin'; ?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Config
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="<?= base_url().'queuing/manageCounters'; ?>">Manage Counters</a>
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#resetModal">Reset Queue</a>
        </div>
      </li>
      <li class="nav-item">
    	<a class="nav-link" href="<?= base_url().'queuing/logout'; ?>">Logout</a>
      </li>
    </ul>
  </div>
</nav>


<div class="container">
  <div class="row" style="margin-top: 5%;">
    <div class="col-1"></div>    
    <div class="col">
      <?php if($this->session->flashdata('message')) { ?>
        <div class="alert alert-success" role="alert">
          <?= $this->session->flashdata('message') ?>
        </div>
      <?php } ?>
      <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal">
        Add Counter
      </button>

      <!-- Add Modal -->
      <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" data-show="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <form method="post" action="<?= base_url().'queuing/addcounter' ?>">
              <div class="modal-header">
                <h5 class="modal-title" id="addModalLabel">Add Counter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <?php if (validation_errors()) { ?>
                    <div class="alert alert-danger" role="alert">
                      <?= validation_errors(); ?>
                    </div>
                <?php } ?>
                <div class="form-group">
                  <label for="counter_number">Counter Number</label>
                  <input type="number" class="form-control" id="counter_number" name="counter_number" placeholder="Counter Number" value="<?= set_value('counter_number'); ?>">                
                </div>
                <div class="form-group">
                  <label for="counter_name">Counter Name</label>
                  <input type="text" class="form-control" id="counter_name" name="counter_name" placeholder="Counter Name" value="<?= set_value('counter_name'); ?>">
                </div>
                <div class="form-group">
                  <label for="counter_manager">Manager Username</label>
                  <input type="text" class="form-control" id="counter_manager" name="counter_manager" placeholder="Counter Manager" value="<?= set_value('counter_manager'); ?>">               
                </div>
                <div class="form-group">
                  <label for="counter_manager_pass">Manager Password</label>
                  <input type="password" class="form-control" id="counter_manager_pass" name="counter_manager_pass" placeholder="Counter Manager Password">               
                </div>
                <div class="form-group">
                  <label for="counter_manager_confpass">Confirm Manager Password</label>
                  <input type="password" class="form-control" id="counter_manager_confpass" name="counter_manager_confpass" placeholder="Confirm Manager Password">               
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>      

      <br><br>

      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Counter Name</th>
            <th scope="col">Manager</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($counters as $counter){ ?>
          <tr>
            <th scope="row"><?= $counter->counter_number; ?></th>
            <td><?= $counter->counter_name; ?></td>
            <td><?= $counter->manager; ?></td>
            <td>
              <a href="#" data-toggle="modal" data-id="<?= $counter->counter_id ?>" data-name="<?= $counter->counter_name ?>" data-target="#editModal" class="open-editModal" >Edit</a>
              <span> | </span>
              <a href="#" data-toggle="modal" data-id="<?= $counter->manager ?>" data-target="#resetPassModal" class="open-resetPassModal" >Reset Password</a>
              <span> | </span>
              <a href="#" data-toggle="modal" data-id="<?= $counter->counter_id ?>" data-target="#deleteModal" class="open-deleteModal" >Delete</a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>

      <!-- Edit Modal -->
      <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" data-show="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <form method="post" action="<?= base_url().'queuing/editcounter' ?>">
              <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit Counter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <?php if (validation_errors()) { ?>
                    <div class="alert alert-danger" role="alert">
                      <?= validation_errors(); ?>
                    </div>
                <?php } ?>
                <div class="form-group">
                  <input type="hidden" id="counter_id" name="counter_id" value="<?php if(validation_errors()){ set_value('counter_id'); } ?>">
                  <label for="counter_name">Counter Name</label>
                  <input type="text" class="form-control" id="counter_name" name="counter_name" placeholder="Counter Name" value="<?= set_value('counter_name'); ?>">
                </div>                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Reset Pass Modal -->
      <div class="modal fade" id="resetPassModal" tabindex="-1" role="dialog" aria-labelledby="resetPassModalLabel" data-show="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <form method="post" action="<?= base_url().'queuing/resetpassword' ?>">
              <div class="modal-header">
                <h5 class="modal-title" id="resetPassModalLabel">Reset Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">                
                <div class="form-group">                  
                  <label>Reset the password of this counter's manager? Password will be the same as username. </label>
                  <input type="hidden" id="counter_manager" name="counter_manager">
                </div>                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Yes</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Delete Modal -->
      <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" data-show="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <form method="post" action="<?= base_url().'queuing/deletecounter' ?>">
              <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Delete Counter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">                
                <div class="form-group">                  
                  <label>Deleting the counter will also delete its manager. Are you sure you want to delete this counter? </label>
                  <input type="hidden" id="del_counter_id" name="del_counter_id">
                </div>                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Yes</button>
              </div>
            </form>
          </div>
        </div>
      </div>

    </div>
    <div class="col-1"></div>
  </div>
</div>

<div class="modal fade" id="resetModal" tabindex="-1" role="dialog" aria-labelledby="resetModalLabel" data-show="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="post" action="<?= base_url().'queuing/resetQueue'; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="resetModalLabel">Reset Queue</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <p>Are you sure you want to reset the queue?</p>
          </div>                
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-success">Yes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
</body>
</html>