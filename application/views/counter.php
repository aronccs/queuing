<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Manage</title>
  <link rel="stylesheet" type="text/css" href="<?= base_url().'styles/counter.css' ?>">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
  <script src="<?= base_url().'scripts/jquery-2.2.0.js'?>" type="text/javascript"></script>
  <link href="https://fonts.googleapis.com/css?family=Stardos+Stencil" rel="stylesheet">
  <script>
    function refresh_content(){
      $.ajax({
            url: "<?= base_url();?>queuing/checkNextNumber",
            type: 'POST',
            dataType: 'JSON',

            success: function(data){                    
                $('#nextNumberReady').html(data)
                setTimeout(function () {
                  $('#error').hide();
                }, 3000);
            }
        });
    };
    setInterval(refresh_content, 1000);
  </script>
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Queuing</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url().'queuing/manage'; ?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#changePassModal">Change Password</a>
      </li>
      <li class="nav-item">        
        <a class="nav-link" href="<?= base_url().'queuing/logout'; ?>">Logout</a>
      </li>
    </ul>
  </div>
</nav>

<!-- If next number is ready to be called -->
<div id="nextNumberReady">
</div>

<?php if($this->session->flashdata('error')){ ?>
<div id="error" class="alert alert-danger" role="alert" style="text-align: center";>
  <?= $this->session->flashdata('error'); ?>
</div>	
<?php } ?>

<?php if($this->session->flashdata('success')){ ?>
<div id="error" class="alert alert-success" role="alert" style="text-align: center";>
  <?= $this->session->flashdata('success'); ?>
</div>  
<?php } ?>


<?php if(validation_errors() || $this->session->flashdata('pass_incorrect')){ ?>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#changePassModal').modal('show');
    });
</script>
<?php } ?>

<?php
  foreach($counter_details as $counter){
?>
  
  <div class="main-col-mng">  
    <div class="box-main-mng">
      <div class="ticket">
        <h3>Ticket</h3>
        <hr style="width: 70%; border-top: 1px solid #8c8b8b; border-bottom: 1px solid #fff;">
        <div class="ticket-main">
          <h1><?= $counter->current_number; ?></h1>
        </div>
        <hr style="width: 70%; margin-top:-4%; border-top: 1px solid #8c8b8b; border-bottom: 1px solid #fff;">
      </div>
      <div class="counter">
        <h3>Counter</h3>
          <hr style="width: 70%; border-top: 1px solid #8c8b8b; border-bottom: 1px solid #fff;">
          <div class="counter-main">
            <h1><?= $counter->counter_number; ?></h1>
			      <h4><?= $counter->counter_name; ?></h4>
          </div>
        <h3></h3>
      </div>
    </div>
  </div>
  <div class="others-col-mng">
    <a class="btn-mng" href="<?= base_url().'queuing/nextnumber/'.$counter->counter_id ?>">Next</a>
    <a class="btn-mng" href="<?= base_url().'queuing/recall/'.$counter->counter_id ?>">Recall</a>
    <!-- <a class="btn-mng" href="">Previous</a> -->
    <!-- <a class="btn-mng" href="#" data-toggle="modal" data-target="#customModal">Custom</a> -->
    <!-- <a class="btn-mng" href="#" data-toggle="modal" data-target="#confirmResetModal">Reset</a> -->
  </div>
  <?php } ?>   

  <!-- Change Password Modal -->
  <?php foreach($user_details as $user){ ?>
  <div class="modal fade" id="changePassModal" tabindex="-1" role="dialog" aria-labelledby="changePassModalLabel" data-show="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form method="post" action="<?= base_url(); ?>queuing/changepassword">
          <div class="modal-header">
            <h5 class="modal-title" id="changePassModalLabel">Change Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <?php if(validation_errors() || $this->session->flashdata('pass_incorrect')){ ?>
              <div id="error" class="alert alert-danger" role="alert" style="text-align: center";>
                <?= $this->session->flashdata('pass_incorrect'); ?>
                <?= validation_errors(); ?>
              </div>  
            <?php } ?>
            <div class="form-group">
              <label for="current_pass">Current Password</label>
              <input type="password" class="form-control" id="current_pass" name="current_pass">
            </div>
            <div class="form-group">
              <label for="new_pass">New Password</label>
              <input type="password" class="form-control" id="new_pass" name="new_pass">
            </div>  
            <div class="form-group">
              <label for="conf_pass">Confirm Password</label>
              <input type="password" class="form-control" id="conf_pass" name="conf_pass">
            </div>              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-success">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <?php } ?>

  <!-- <div class="modal fade" id="customModal" tabindex="-1" role="dialog" aria-labelledby="customModalLabel" data-show="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form method="post" action="">
          <div class="modal-header">
            <h5 class="modal-title" id="customModalLabel">Custom Counter Number</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="custom_number">Custom Number</label>
              <input type="number" class="form-control" id="custom_number" name="custom_number" min="1" max="9999" value="">
            </div>                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade" id="confirmResetModal" tabindex="-1" role="dialog" aria-labelledby="confirmResetModalLabel" data-show="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form method="post" action="">
          <div class="modal-header">
            <h5 class="modal-title" id="confirmResetModalLabel">Reset Counter Number</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <p>Are you sure you want to reset the counter number?</p>
            </div>                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-success">Yes</button>
          </div>
        </form>
      </div>
    </div>
  </div> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
</body>
</html>