<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Queuing System</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?= base_url().'styles/homepage.css' ?>">	
</head>
<body>

<div class="main-row">
	<div class="main-left-col">
		<div class="main-left-content">
			<a href="<?= base_url().'queuing/viewing' ?>" class="btn btn-outline-dark btn-lg btn-block">Viewing</a>
			<br><br>
			<h2>Login to your account</h2>
			<form method="post" action="<?= base_url().'queuing/verify' ?>">
			  <div class="form-group">
			    <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?= set_value('username') ?>">
			     <?php if(form_error('username')){ ?>
				 	<small id="uhelp" class="form-text text-danger"><?= form_error('username'); ?></small>
				 <?php } ?>
			  </div>		 
			  <div class="form-group">
			    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
			     <?php if(form_error('password')){ ?>
				 	<small id="uhelp" class="form-text text-danger" style="color: red;"><?= form_error('password'); ?></small>
				 <?php } ?>
			  </div>	  
			  <button type="submit" class="btn btn-danger float-right">Submit</button>
			</form>
			<?php 
				if ($this->session->flashdata('error')) {
	  				echo "<p class='form-text text-danger'>".$this->session->flashdata('error')."</p>";
			 	} 
			?>		
		</div>
	</div>
	<div class="main-right-col">
		<div class="main-right-content">
			<img src="<?= base_url(); ?>images/logo.png" alt="logo" style="width: 100%; height: 100%;">	
		</div>
	</div>
</div>
<div class="footer-row">
	<div class="footer-content">
		<p class="tagline">Queuing your way for better service.</p>
		<div class="footer-tagline">
			<img src="<?= base_url(); ?>images/logo.png" alt="logo" style="width: 50%; height: 50%;">	
		</div>
	</div>
</div>

</body>
</html>