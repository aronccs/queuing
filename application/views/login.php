<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Login</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
</head>
<body>

<div class="container">
	<div class="row" style="margin-top: 10%;">
		<div class="col"></div>
		<div class="col-4">
			<?php if (validation_errors() || $this->session->flashdata('error')) { ?>
			<div class="alert alert-danger" role="alert">
	  			<?php 
	  				echo validation_errors(); 
	  				echo $this->session->flashdata('error'); 
	  			?>
			</div>
			<?php } ?>
			<form method="post" action="<?= base_url().'queuing/verify' ?>">
			  <div class="form-group">
			    <label for="username">Username</label>
			    <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?= set_value('username') ?>">		    
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1">Password</label>
			    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
			  </div>
			  <a href="<?= base_url(); ?>"><button type="button" class="btn btn-danger">Back</button></a>
			  <button type="submit" class="btn btn-success">Login</button>
			</form>
		</div>
		<div class="col"></div>
	</div>
</div>

</body>
</html>