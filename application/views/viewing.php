<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<!-- <meta http-equiv="refresh" content="6" > -->
	<meta charset="utf-8">
	<title>Queuing System</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url().'styles/viewing.css' ?>">
	<script src='https://code.responsivevoice.org/responsivevoice.js'></script>
	<script src="<?= base_url().'scripts/jquery-2.2.0.js'?>" type="text/javascript"></script>
	<link href="https://fonts.googleapis.com/css?family=Stardos+Stencil" rel="stylesheet">
	<script>
		function refresh_content(){
			$.ajax({
		        url: "<?= base_url();?>queuing/getQueue",
	        	type: 'POST',
	        	dataType: 'JSON',

	        	success: function(data){                    
	            	$('#content').html(data)
	        	}
		    });
		};
		setInterval(refresh_content, 1000);
	</script>
</head>
<body>
<div id="content" style="height: 100%;">	
</div>
</body>
</html>