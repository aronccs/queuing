<div class="main-col">
	<div class="box-main">		
		<?php foreach($latest as $last){ ?>	
		<div class="ticket">
			<?php if($new_data){ ?>
			<audio id="bell" hidden>
		    	<source src="<?= base_url().'sounds/bell.mp3' ?>" type="audio/mpeg">
		    </audio>
			<script type="text/javascript">
				var bell = document.getElementById("bell");
				var label = document.getElementById("current_number");
				var current_number = label.dataset.number;

				if(current_number > 0){
					setTimeout(function(){ bell.play() }, 1000);
			   		setTimeout("responsiveVoice.speak('Customer number ' + <?= $last->current_number; ?> + ' please proceed to counter ' + <?= $last->counter_number; ?>)", 3000);
			   	}
			</script>			
			<?php } ?>
			<h3>TICKET</h3>
			<hr style="width: 70%; border-top: 1px solid #8c8b8b; border-bottom: 1px solid #fff;">
			<div class="ticket-main">
				<h1 id="current_number" data-number="<?= $last->current_number; ?>"><?= $last->current_number; ?></h1>				
			</div>
			<hr style="width: 70%; border-top: 1px solid #8c8b8b; border-bottom: 1px solid #fff;">
		</div>
		<div class="counter">
			<h3>Please proceed to counter</h3>
			<hr style="width: 70%; border-top: 1px solid #8c8b8b; border-bottom: 1px solid #fff;">
			<div class="counter-main">
				<h1><?= $last->counter_number; ?></h1>
				<h4><?= strtoupper($last->counter_name); ?></h4>
			</div>			
		</div>
		<?php } ?>
	</div>
</div>
<div class="others-col">
	<div class="time">
		<p><?= strtoupper(date("M. j, Y - g:i A")); ?></p>
	</div>
	<?php foreach($others as $counters){ ?>
	<div class="box-others">
		<table class="table" style="width: 100%;">
			<tbody>
				<tr style="line-height: 10%;">					
					<td style="width: 50%; vertical-align: top;">
						<p class="title">TICKET</p>							
					</td>
					<td style="width: 50%; text-align: right;">
						<p class="number"><?= $counters->current_number; ?></p>
					</td>					
				</tr>
				<tr style="line-height: 10%;">
					<td>
						<p class="title">COUNTER</p>						
					</td>
					<td style="text-align: right;">
						<p class="number"><?= $counters->counter_number; ?></p>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<?php } ?>
</div>