<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Queuing extends CI_Controller {

	public function index()
	{
		$username =	$this->session->userdata('username');

		if(isset($username)){
			$this->load->model('QueuingModel');
			$user_level = $this->QueuingModel->checkLevel($username);

			if($user_level == 1){
				$this->session->set_userdata('user_level', 'admin');
				redirect('Queuing/admin', 'refresh');
			} elseif ($user_level == 2){       				
				$this->session->set_userdata('user_level', 'manager');
				redirect('Queuing/manage', 'refresh');
			}
		} else {
			$this->load->helper('form');
			$this->load->view('index');
		}
	}

	public function viewing(){			
		$this->load->view('viewing');
	}

	//For AJAX
	public function getQueue(){
		$this->load->model('QueuingModel');
		$last_updated = $this->QueuingModel->lastUpdatedCounter();
		foreach($last_updated as $lu){
			$current_counter = $lu->counter_id;
			$current_number = $lu->current_number;
			$current_time = $lu->last_updated;
		}

		$checker = $this->checker($current_counter, $current_number, $current_time);
		if($checker){
			$data['new_data'] = true;
		} else {
			$data['new_data'] = false;
		}

		$data['latest'] = $last_updated;
		$data['others'] = $this->QueuingModel->otherCounters();
		$html = $this->load->view('queue', $data, true);
		echo json_encode($html);
	}

	//Check if the current counter has been changed
	public function checker($next_counter, $next_number, $next_time){
		$prev_counter = $this->session->userdata('current_counter');
		$prev_number = $this->session->userdata('current_number');
		$prev_time = $this->session->userdata('current_time');

		//If the queue is run for the first time
		if(!isset($prev_counter) || !isset($prev_number) || !isset($prev_time)){
			$this->session->set_userdata('current_counter', $next_counter);
			$this->session->set_userdata('current_number', $next_number);
			$this->session->set_userdata('current_time', $next_time);

			return true;
		
		} else {
		
			if($next_counter != $prev_counter || $next_number != $prev_number || $next_time != $prev_time){
				$this->session->set_userdata('current_counter', $next_counter);
				$this->session->set_userdata('current_number', $next_number);
				$this->session->set_userdata('current_time', $next_time);

				return true;
			} else {
				return false;
			}
		}
	}	

	public function verify(){
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE){
            $this->load->view('index');
        } else {
        	$this->load->model('QueuingModel');

        	$username = $this->input->post('username');
        	$password = $this->input->post('password');
        	$user_count = $this->QueuingModel->checkUser($username);

        	if($user_count <= 0){
        		$this->session->set_flashdata('error', 'User does not exist.');
        		$this->load->view('index');
        	} else {
        		$stored_password = $this->QueuingModel->verify($username);
        		
        		if (sha1($password) != $stored_password) {
        			$this->session->set_flashdata('error', 'Incorrect username or password.');
        			$this->load->view('index');
        		} else {
        			$user_id = $this->QueuingModel->getUserId($username);
        			$this->session->set_userdata('user_id', $user_id);
        			$this->session->set_userdata('username', $username);

        			$user_level = $this->QueuingModel->checkLevel($username);

        			if($user_level == 1){
        				$this->session->set_userdata('user_level', 'admin');
        				redirect('Queuing/admin', 'refresh');
        			} elseif ($user_level == 2){       				
        				$this->session->set_userdata('user_level', 'manager');
        				redirect('Queuing/manage', 'refresh');
        			}
        		}
        	}
        }
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// 								ADMIN
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	public function admin(){
		if(!$this->check_if_admin()){			
			redirect('Queuing/index');
		} else {
			$this->load->view('admin');
		}
	}

	public function manageCounters(){
		$this->load->helper('form');
		if(!$this->check_if_admin()){
			redirect('Queuing/index');
		} else {
			$this->load->model('QueuingModel');
			$data['counters'] = $this->QueuingModel->getCounters();
			$this->load->view('manage_counters', $data);
		}
	}

	public function addCounter(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('QueuingModel');

		$this->form_validation->set_rules('counter_number', 'counter number', 'required|numeric|is_unique[counters.counter_number]');
		$this->form_validation->set_rules('counter_name', 'counter name', 'required|alpha_numeric_spaces');
		$this->form_validation->set_rules('counter_manager', 'counter manager', 'required|alpha_numeric|is_unique[users.username]');
		$this->form_validation->set_rules('counter_manager_pass', 'counter manager password', 'required|min_length[8]|max_length[32]');
		$this->form_validation->set_rules('counter_manager_confpass', 'confirm password', 'required|matches[counter_manager_pass]');

		$this->form_validation->set_message('is_unique', 'Please choose another {field} as it already exists.');

		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('isAdd', 'true');
			$data['counters'] = $this->QueuingModel->getCounters();
			$this->load->view('manage_counters', $data);
		} else {			
			$this->QueuingModel->insert();			
			$this->session->set_flashdata('message', 'Counter and user has been added.');
			redirect('Queuing/managecounters');
		}
	}

	public function editCounter(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('QueuingModel');

		$this->form_validation->set_rules('counter_name', 'Counter Name', 'required|alpha_numeric_spaces');

		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('isEdit', 'true');
			$data['counters'] = $this->QueuingModel->getCounters();
			$this->load->view('manage_counters', $data);
		} else {
			$counter_id = $this->input->post('counter_id');
			$counter_name = $this->input->post('counter_name');
			$this->QueuingModel->updateCounter($counter_id, $counter_name);
			
			$this->session->set_flashdata('message', 'Counter has been updated.');
			redirect('Queuing/managecounters');
		}
	}

	public function resetPassword(){
		$this->load->model('QueuingModel');

		$username = $this->input->post('counter_manager');
		$this->QueuingModel->resetPassword($username);

		$this->session->set_flashdata('message', 'Password has been reset.');
		redirect('Queuing/managecounters');
	}

	public function deleteCounter(){
		$this->load->model('QueuingModel');

		$counter_id = $this->input->post('del_counter_id');
		$this->QueuingModel->deleteCounter($counter_id);

		$this->session->set_flashdata('message', 'Counter has been deleted.');
		redirect('Queuing/managecounters');
	}

	public function resetQueue(){
		if(!$this->check_if_admin()){
			redirect('Queuing/index');
		} else {
			$this->load->model('QueuingModel');
			$this->QueuingModel->resetQueue();
			redirect('Queuing/admin');
		}
	}

	public function check_if_admin(){
		$user_level = $this->session->userdata('user_level');
		if(!isset($user_level) || $user_level != 'admin'){
			return false;
		} else {
			return true;
		}
	}
	//END OF ADMIN SECTION


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// 								MANAGER
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	public function manage(){
		if(!$this->check_if_manager()){			
			redirect('Queuing/index');
		} else {
			$this->load->model('QueuingModel');
			$this->load->helper('form');

			$user_id = $this->session->userdata('user_id');
			$username = $this->session->userdata('username');
			$counter_id = $this->QueuingModel->getCounterOfManager($username);
			
			$data['user_details'] = $this->QueuingModel->getUserDetails($username);
			$data['counter_details'] = $this->QueuingModel->getCounterDetails($counter_id);
			$this->load->view('counter', $data);
		}
	}

	public function checkNextNumber(){
		$this->load->model('QueuingModel');
		
		$username = $this->session->userdata('username');
		$counter_id = $this->QueuingModel->getCounterOfManager($username);
		$counter_number = $this->QueuingModel->getCounterNumber($counter_id);

		if($counter_number == 1){
			$html = $this->load->view('next_number_empty', '', true);
		
		} else {

			$current_number = $this->QueuingModel->getCounterCurrentNumber($counter_id);
			$next_number = $this->QueuingModel->getLastCounterLastNumber($counter_id);

			if($next_number > $current_number){
				$html = $this->load->view('next_number', '', true);
			} else {
				$html = $this->load->view('next_number_empty', '', true);
			}
		}

		echo json_encode($html);
	}

	public function nextNumber($counter_id){
		if(!$this->check_if_manager()){			
			redirect('Queuing/index');
		} else {
			$this->load->model('QueuingModel');
			$current_number = $this->QueuingModel->getCounterCurrentNumber($counter_id);		
			$num_rows = $this->QueuingModel->checkNumberUnique($current_number);
			$counter_number = $this->QueuingModel->getCounterNumber($counter_id);
			$last_counter_number = $this->QueuingModel->getLastCounterLastNumber($counter_id);

			if($num_rows > 0){
				$this->session->set_flashdata('error', 'Next ticket number is still in previous counter.');
				
			} else {				

				if($counter_number == 1){
					$this->QueuingModel->setCounterCurrentNumberNext($counter_id, $current_number);
					$this->QueuingModel->setCounterLastNumber($counter_id, $current_number);

				} elseif($counter_number > 1 && isset($last_counter_number)){
					//$current_number = ($last_counter_number - 1);

					$this->QueuingModel->setCounterCurrentNumberNext($counter_id, $current_number);
					$this->QueuingModel->setCounterLastNumber($counter_id, $current_number);
				}
				
			}
				redirect('Queuing/manage');
		}
	}

	public function recall($counter_id){
		if(!$this->check_if_manager()){			
			redirect('Queuing/index');
		} else {
			$this->load->model('QueuingModel');			
			$this->QueuingModel->setCounterCurrentNumberRecall($counter_id);		
			
			redirect('Queuing/manage');
		}
	}

	public function previousNumber($counter_id){
		if(!$this->check_if_manager()){			
			redirect('Queuing/index');
		} else {
			$this->load->model('QueuingModel');
			$current_number = $this->QueuingModel->getCounterCurrentNumber($counter_id);

			if($current_number > 0){
				$this->QueuingModel->setCounterCurrentNumberPrevious($counter_id, $current_number);	
			}

			redirect('Queuing/manage');
		}
	}

	public function resetCounter($counter_id){
		if(!$this->check_if_manager()){			
			redirect('Queuing/index');
		} else {
			$this->load->model('QueuingModel');
			$this->QueuingModel->resetCounterNumber($counter_id);	
			
			redirect('Queuing/manage');
		}
	}

	public function customCounterNumber($counter_id){
		if(!$this->check_if_manager()){			
			redirect('Queuing/index');
		} else {
			$this->load->model('QueuingModel');
			$custom_number = $this->input->post('custom_number');
			$this->QueuingModel->customCounterNumber($counter_id, $custom_number);	
			
			redirect('Queuing/manage');
		}
	}

	public function changePassword(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('QueuingModel');

		$username = $this->session->userdata('username');
		$stored_pass = $this->QueuingModel->verify($username);
		$password = $this->input->post('current_pass');

		if(sha1($password) != $stored_pass){
			$this->session->set_flashdata('pass_incorrect', 'Password incorrect.');
			redirect('Queuing/manage');
		} else {
			$this->form_validation->set_rules('new_pass', 'new password', 'required|min_length[8]');
			$this->form_validation->set_rules('conf_pass', 'confirm password', 'required|matches[new_pass]');

			if ($this->form_validation->run() == FALSE){
				$user_id = $this->session->userdata('user_id');
				$username = $this->session->userdata('username');
				$counter_id = $this->QueuingModel->getCounterOfManager($username);
				
				$data['user_details'] = $this->QueuingModel->getUserDetails($username);
				$data['counter_details'] = $this->QueuingModel->getCounterDetails($counter_id);
				$this->load->view('counter', $data);

			} else {

				$this->QueuingModel->updatePassword($username);
				$this->session->set_flashdata('success', 'Your password has been updated.');
				redirect('Queuing/manage');
			}
		}
	}

	public function check_if_manager(){
		$user_level = $this->session->userdata('user_level');
		if(!isset($user_level) || $user_level != 'manager'){
			return false;
		} else {
			return true;
		}
	}


	public function logout(){
		$this->session->sess_destroy();
		redirect('Queuing/index');
	}
}
