<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QueuingModel extends CI_Model {

	public function lastUpdatedCounter(){
		$this->db->select('*');
		$this->db->from('counters');
		$this->db->order_by('last_updated', 'DESC');
		$this->db->limit(1);

		$query = $this->db->get();
		return $query->result();
	}

	public function otherCounters(){
		$query = $this->db->query('
				    SELECT *
				    FROM counters
				    WHERE counter_id NOT IN (select counter_id from (
				        SELECT *
				        FROM counters				        
				        ORDER BY last_updated DESC
				        LIMIT 1) temp_tab)
				    ORDER BY last_updated DESC
				');

		return $query->result();
	}

	public function checkUser($username){
		$this->db->select('*');	
		$this->db->from('users');
		$this->db->where('username',$username);

		$query = $this->db->get();
		return $query->num_rows();
	}

	public function verify($username){
		$query = $this->db->query("SELECT password FROM users WHERE username='$username';");
		return $query->row()->password;
	}

	public function getUserDetails($username){
		$this->db->select('*');	
		$this->db->from('users');
		$this->db->where('username',$username);

		$query = $this->db->get();
		return $query->result();
	}

	public function getUserId($username){
		$query = $this->db->query("SELECT user_id FROM users WHERE username='$username';");
		return $query->row()->user_id;
	}

	public function checkLevel($username){
		$query = $this->db->query("SELECT user_type FROM users WHERE username='$username';");
		return $query->row()->user_type;
	}


	//ADMIN SECTION
	public function getCounters(){
		$this->db->select('*');	
		$this->db->from('counters');
		$this->db->order_by('counter_number');

		$query = $this->db->get();
		return $query->result();
	}

	public function insert(){
		$counter = array(
			'counter_number' => $this->input->post('counter_number'),
			'counter_name' => $this->input->post('counter_name'),
			'manager' => $this->input->post('counter_manager')
			);
		$this->db->insert('counters', $counter);

		$user = array(
			'username' => $this->input->post('counter_manager'),
			'password' => sha1($this->input->post('counter_manager_pass')),
			'user_type' => 2
			);
		$this->db->insert('users', $user);
	}

	public function updateCounter($counter_id, $counter_name){
		$this->db->set('counter_name', $counter_name);
		$this->db->where('counter_id', $counter_id);
		$this->db->update('counters');
	}

	public function resetPassword($username){
		$this->db->set('password', sha1($username));
		$this->db->where('username', $username);
		$this->db->update('users');
	}

	public function deleteCounter($counter_id){
		$this->db->select('manager');
		$this->db->from('counters');
		$this->db->where('counter_id', $counter_id);
		$query = $this->db->get();
		$manager = $query->row()->manager;

		//delete user
		$this->db->where('username', $manager);
		$this->db->delete('users');

		//delete counter
		$this->db->where('counter_id', $counter_id);
		$this->db->delete('counters');
	}

	public function resetQueue(){
		$this->db->set('current_number', 0);
		$this->db->set('last_number', null);
		$this->db->set('last_updated', date('Y-m-d H:i:s'));
		$this->db->update('counters');
	}
	//END OF ADMIN SECTION


	//MANAGER SECTION
	public function getCounterOfManager($manager){
		$this->db->select('counter_id');
		$this->db->from('counters');
		$this->db->where('manager', $manager);
		$query = $this->db->get();
		return $query->row()->counter_id;
	}

	public function getCounterDetails($counter_id){
		$this->db->select('*');
		$this->db->from('counters');
		$this->db->where('counter_id', $counter_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function getCounterNumber($counter_id){
		$this->db->select('counter_number');
		$this->db->from('counters');
		$this->db->where('counter_id', $counter_id);
		$query = $this->db->get();
		return $query->row()->counter_number;
	}

	public function getCounterCurrentNumber($counter_id){
		$query = $this->db->query("SELECT current_number FROM counters WHERE counter_id=$counter_id;");
		return $query->row()->current_number;
	}
	
	public function checkNumberUnique($number){
		$this->db->select('current_number');	
		$this->db->from('counters');
		$this->db->where('current_number',$number+1);

		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function getLastCounterLastNumber($counter_id){
		$this->db->select('last_number');
		$this->db->from('counters');
		$this->db->where('counter_id', $counter_id - 1);
		$query = $this->db->get();
		return $query->row()->last_number;
	}
	
	public function setCounterLastNumber($counter_id, $current_number){
		$this->db->set('last_number', $current_number);
		$this->db->set('last_updated', date('Y-m-d H:i:s'));
		$this->db->where('counter_id', $counter_id);
		$this->db->update('counters');
	}

	public function setCounterCurrentNumberNext($counter_id, $current_number){
		$this->db->set('current_number', $current_number + 1);
		$this->db->set('last_updated', date('Y-m-d H:i:s'));
		$this->db->where('counter_id', $counter_id);
		$this->db->update('counters');
	}

	public function setCounterCurrentNumberRecall($counter_id){
		$this->db->set('last_updated', date('Y-m-d H:i:s'));
		$this->db->where('counter_id', $counter_id);
		$this->db->update('counters');
	}

	public function setCounterCurrentNumberPrevious($counter_id, $current_number){
		$this->db->set('current_number', $current_number - 1);
		$this->db->set('last_updated', date('Y-m-d H:i:s'));
		$this->db->where('counter_id', $counter_id);
		$this->db->update('counters');
	}

	public function resetCounterNumber($counter_id){
		$this->db->set('current_number', 0);
		$this->db->set('last_updated', date('Y-m-d H:i:s'));
		$this->db->where('counter_id', $counter_id);
		$this->db->update('counters');
	}

	public function customCounterNumber($counter_id, $custom_number){
		$this->db->set('current_number', $custom_number);
		$this->db->set('last_updated', date('Y-m-d H:i:s'));
		$this->db->where('counter_id', $counter_id);
		$this->db->update('counters');
	}

	public function updatePassword($username){
		$password = $this->input->post('new_pass');

		$this->db->set('password', sha1($password));
		$this->db->where('username', $username);
		$this->db->update('users');
	}
	//END OF MANAGER SECTION
}

?>